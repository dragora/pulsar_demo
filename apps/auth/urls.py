"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import RedirectView
from django.views.generic.edit import CreateView
from django.contrib.auth import views as auth_views

from forms import SignUpForm, LoginForm

from views import LoginView, LogoutView, SignupView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='login', permanent=True), name='auth'),
    # url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^login/$',
        auth_views.login, {'template_name': 'login.html', 'authentication_form': LoginForm, 'redirect_field_name': '/'},
        name='login'),
    url(r'^logout$', LogoutView.as_view(), name='logout'),
    # url(r'^signup', SignupView.as_view(), name='signup'),
    url(r'^signup', CreateView.as_view(
        # TODO: make a proxy success page with the redirect to the login page
        template_name='signup.html', form_class=SignUpForm, success_url='/auth/login'
    ), name='signup'),
    # url(r'^oauth2callback', GoogleAuthReturnView.as_view(), name='oauth2callback'),
]
