# coding=utf-8

from django.db import models
from django.contrib.auth.models import User

from apps.events.models import Tag

__author__ = 'dragora'


class UserData(models.Model):
    id = models.OneToOneField(User, primary_key=True)
    tags_subscribed = models.ManyToManyField(to=Tag, blank=True)

    def __unicode__(self):
        return self.id.username