# coding=utf-8
__author__ = 'dragora'

from django.db.models.signals import post_save
from django.contrib.auth.models import User

from models import UserData


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserData.objects.get_or_create(id=instance)

post_save.connect(create_user_profile, sender=User)