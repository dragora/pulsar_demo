# coding=utf-8
__author__ = 'dragora'

from django.apps import AppConfig


class AuthUsersConfig(AppConfig):
    name = 'apps.auth'
    label = 'auth_users'

    def ready(self):
        import signals