# coding=utf-8
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

__author__ = 'dragora'


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        # self.helper.form_id = 'id-exampleForm'
        self.helper.form_class = 'form-login form-wrapper form-narrow'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.field_class = 'form-group'

        self.helper.add_input(Submit('submit', 'Log In'))


class SignUpForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        # self.helper.form_id = 'id-exampleForm'
        self.helper.form_class = 'form-login form-wrapper form-medium'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.field_class = 'form-group'

        self.helper.add_input(Submit('submit', 'Sign Up'))