# coding=utf-8

from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView

from views import FeedManagerView

__author__ = 'dragora'

urlpatterns = [
    url(r'^manager/$', login_required(FeedManagerView.as_view()), name='feed_manager'),
]