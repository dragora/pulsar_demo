from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from models import Tag
from forms import TagsForm

# Create your views here.


class FeedManagerView(TemplateView):
    def get(self, request, *args, **kwargs):

        tags_tree = Tag.objects.all()

        form = TagsForm()

        return render(request, 'feed_manager.html', {
            'tags_tree': tags_tree,

            'form': form
        })

    def post(self, request, *args, **kwargs):
        print 'Request POST data:\n', request.POST

        form = TagsForm(request.POST)
        user = request.user

        if form.is_valid():
            user.userdata.tags_subscribed = Tag.objects.filter(name__in=form.cleaned_data['tags_selected'])
        else:
            pass

        return redirect('/')