# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Event name')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('time_start', models.DateTimeField(verbose_name=b'Starting at')),
                ('time_end', models.DateTimeField(verbose_name=b'Ending at')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Tag name')),
                ('parent_tag', models.ForeignKey(verbose_name=b'Parent tag', to='events.Tag')),
            ],
        ),
    ]
