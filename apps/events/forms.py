# coding=utf-8

from django import forms

from models import Tag

__author__ = 'dragora'


class TagsForm(forms.Form):
    tags_selected = forms.MultipleChoiceField(
        choices=[(tag.name, tag.name) for tag in Tag.objects.all()], widget=forms.CheckboxSelectMultiple)