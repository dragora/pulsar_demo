from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from models import Tag, Event

# Register your models here.

admin.site.register(Tag, MPTTModelAdmin)
admin.site.register(Event)