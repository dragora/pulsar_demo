from django.db import models

from mptt.models import MPTTModel, TreeForeignKey

# Create your models here.


class Tag(MPTTModel):
    name = models.CharField(verbose_name='Tag name', max_length=50, unique=True)
    parent = TreeForeignKey(to='self',
                            null=True, blank=True, verbose_name='Parent tag', related_name='Children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(verbose_name='Event name', max_length=50)
    description = models.TextField(verbose_name='Description')
    time_start = models.DateTimeField(verbose_name='Starting at')
    time_end = models.DateTimeField(verbose_name='Ending at')