django==1.8.3
simplejson
gunicorn
psycopg2
Pillow
django-imagekit
django-crispy-forms
django-flat-theme

django-tastypie
django-angular

django-mptt